'''                     
    Xadrez Computacional
    **TicTacToe Game**
    Natalia Gil e Pedro Costa  
                                '''

'''
    Como jogar:
        - O usuário é sempre o primeiro a jogar e suas jogadas são marcadas no tabuleiro com 'X'.
        - O jogo recebe como entrada o click com o botão esquerdo do mouse, logo, é necessário que 
        um único click seja realizado no centro da região de interesse do tabuleiro 3x3, para que 
        não ocorram movimentos indesejados.
        - O jogo abre a arvóre minmax a partir de cada jogada, então, é um comportamento normal que a 
        primeira jogada do computador leve mais tempo para ser processada do que as demais.
        - O jogo reinicia automaticamente ao final das jogadas ou quando há um vencedor, mas pode ser 
        interrompido ao fechar a janela do PyGame.
    '''


# setup
import os
# Set the QT_DEBUG_PLUGINS environment variable
os.environ['QT_DEBUG_PLUGINS'] = '1'
import numpy as np
#from matplotlib import pyplot as plt
import pygame
from pygame.locals import *

pygame.init()
clock = pygame.time.Clock()
fps = 30

# Constants
WIDTH, HEIGHT = 900, 850
SCREEN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Tic-Tac-Toe")

BOARD = pygame.image.load("tictactoe_board.png")
X_IMG = pygame.image.load("tictactoe_X.png")
O_IMG = pygame.image.load("tictactoe_O.png")
BG_COLOR = (255, 255, 255)

# Game variables
current_player = "X" # start

# 3x3 board
board = [[0, 0, 0],
         [0, 0, 0], 
         [0, 0, 0]]
graphical_board = [[[None, None], [None, None], [None, None]], 
                    [[None, None], [None, None], [None, None]], 
                    [[None, None], [None, None], [None, None]]]


SCREEN.fill(BG_COLOR)
SCREEN.blit(BOARD, (64, 64))

pygame.display.update()

# the human player
def render_board(board, ximg, oimg):
    global graphical_board
    for i in range(3):
        for j in range(3):
            if board[i][j] == 'X':
                # Create an X image and rect
                graphical_board[i][j][0] = ximg
                graphical_board[i][j][1] = ximg.get_rect(center=(j*300+150, i*300+150))
            elif board[i][j] == 'O':
                graphical_board[i][j][0] = oimg
                graphical_board[i][j][1] = oimg.get_rect(center=(j*300+150, i*300+150))

def add_XO(board, graphical_board, current_player):
    current_pos = pygame.mouse.get_pos()
    converted_x = (current_pos[0]-65)/835*2
    converted_y = current_pos[1]/835*2
    if board[round(converted_y)][round(converted_x)] != 'O' and board[round(converted_y)][round(converted_x)] != 'X':
        board[round(converted_y)][round(converted_x)] = current_player
        if current_player == 'O':
            current_player = 'X'
        else:
            current_player = 'O'
    
    render_board(board, X_IMG, O_IMG)

    for i in range(3):
        for j in range(3):
            if graphical_board[i][j][0] is not None:
                SCREEN.blit(graphical_board[i][j][0], graphical_board[i][j][1])
    
    return board, current_player

# the machine player
def add_XO_minmax(board, graphical_board, current_player):
    board_2 = np.array(np.copy(board))
    board_2 = np.where(board_2 == 'X', -1, np.where(board_2 == 'O', 1, 0))

    idx_best = find_best_move(board, current_player)
    board[idx_best[0]][idx_best[1]] = current_player

    if current_player == 'O':
        current_player = 'X'
    else:
        current_player = 'O'

    render_board(board, X_IMG, O_IMG)

    for i in range(3):
        for j in range(3):
            if graphical_board[i][j][0] is not None:
                SCREEN.blit(graphical_board[i][j][0], graphical_board[i][j][1])
    return board, current_player

# check positions
def _checar_posicao(linha):
    elemento = linha[0]
    are_equal = np.all(linha == elemento)
    if are_equal:
        if elemento == 1:
            return 1
        elif elemento == -1:
            return -1
    return 0

# check winner
def checar_vencedor(board): 
    board = np.array(np.copy(board))
    board = np.where(board == 'X', -1, np.where(board == 'O', 1, 0))
    vencedor = 0
    for i in range(3):
        linha = board[i]
        coluna = board.T[i]
        vencedor_linha = _checar_posicao(linha)
        vencedor_coluna = _checar_posicao(coluna)

        if vencedor_linha != 0:
            vencedor = vencedor_linha
            break
        if vencedor_coluna != 0:
            vencedor = vencedor_coluna
            break
    
    diag = np.diagonal(board)
    
    vencedor_diag = _checar_posicao(diag)
    if vencedor_diag != 0:
        vencedor = vencedor_diag
        

    diag_inv = np.diagonal(np.fliplr(board))
    vencedor_diag_inv = _checar_posicao(diag_inv)
    if vencedor_diag_inv != 0:
        vencedor = vencedor_diag_inv

    posicoes_livres = np.where(board == 0)
    if len(posicoes_livres[0]) == 0:
        if vencedor == 0:
            vencedor = 2
    return vencedor

# evaluate game status
def evaluate(board):
    # Check if O or X has won the game
    # Return +10 for O win, -10 for X win, 0 for a draw, or None for game ongoing
    if checar_vencedor(board) == 1:  # Check if O (1) has won
        return 10
    elif checar_vencedor(board) == -1:  # Check if X (-1) has won
        return -10
    elif checar_vencedor(board) == 0:  # Check if X (-1) has won
        return None
    else:  
        return 0 #(draw)

# opens the tree
def minimax(board, depth, is_maximizer):
    board = np.copy(board)
    score = evaluate(board)

    if score is not None:
        return score

    if is_maximizer:
        max_score = -float("inf")
        for i in range(3):
            for j in range(3):
                if board[i][j] == '0':
                    board[i][j] = 'O'
                    max_score = max(max_score, minimax(board, depth + 1, False))
                    board[i][j] = '0'
        return max_score
    else:
        min_score = float("inf")
        for i in range(3):
            for j in range(3):
                if board[i][j] == '0':
                    board[i][j] = 'X'
                    min_score = min(min_score, minimax(board, depth + 1, True))
                    board[i][j] = '0'
        return min_score

# find best moves acording to minmax strategy
def find_best_move(board,current_player):
    board = np.copy(board)
    best_move = (-1, -1)
    best_score = -float("inf")

    for i in range(3):
        for j in range(3):
            if board[i][j] == '0':
                board[i][j] = current_player
                move_score = minimax(board, 0, False)
                board[i][j] = 0

                if move_score > best_score:
                    best_score = move_score
                    best_move = (i, j)

    return best_move

game_finished = False

# Game Loop
running = True
bot = 0
while running:
    clock.tick(fps)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            # update board with the human input
            board, current_player = add_XO(board, graphical_board, current_player)
            pygame.display.update()
            # update board with the machine player
            board, current_player = add_XO_minmax(board, graphical_board, current_player)
            pygame.time.delay(1000)
            print(board)

            if checar_vencedor(board):
                game_finished = True 
            pygame.display.flip()
            bot = 0
            if game_finished:
                
                board = [['0', '0', '0'], ['0', '0', '0'], ['0', '0', '0']]
                graphical_board = [[[None, None], [None, None], [None, None]], 
                                    [[None, None], [None, None], [None, None]], 
                                    [[None, None], [None, None], [None, None]]]

                current_player = 'X'

                SCREEN.fill(BG_COLOR)
                SCREEN.blit(BOARD, (64, 64))

                game_finished = False
                pygame.time.delay(1000)
                pygame.display.update()